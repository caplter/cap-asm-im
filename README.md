# cap-asm-im

CAP LTER All Scientists Meeting Information Management poster

Previous posters are stored in PDF format in the `archive` directory.

## workflow

posterdown Rmd --> knit to html --> edit hmtl<sup>1</sup> --> open with Chrome<sup>2</sup> --> print to PDF

This is an iterative process as poster details will change slightly, sometimes not so slightly, when previewing the poster in different browsers, then when printing the HTML.

<sup>1</sup> as necessary, there are little bugs in posterdown (e.g., a seeming inability to remove commas from after the institution number) that can be and are more easily addresed in the html file directly  
<sup>2</sup> seems to work best with Chrome
