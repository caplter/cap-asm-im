﻿Earl, S. R.1


The CAP LTER addresses the challenges of curating and disseminating research data with a robust Information Management System (IMS) that benefits CAP LTER investigators and the broader scientific community. The CAP LTER IMS facilitates the ingestion of research data and metadata from the project's long-term monitoring programs and project investigators. CAP LTER data are archived with the Environmental Data Initiative, which provides long-term storage and access to research data. Research data are also cataloged by the DataONE federation, which greatly enhances their discoverability. Through these resources, data from the CAP LTER’s long-term monitoring programs and project investigators are available to the community as building blocks for future research efforts. For project investigators, submitting data through the CAP LTER meets the data publishing requirements set by funders and publishers, and each data set is given a citation with a Digital Object Identifier (DOI) that a data provider may reference. The CAP LTER Information Manager is available to assist with the data publishing process, data management plans, and generally to provide guidance regarding effective approaches to managing research data.


1Global Futures Laboratory, Arizona State University, University, PO Box 875402, Tempe, AZ 85287-5402;


Earl, S. R. CAP LTER informatics: data management for project investigators and the scientific community