---
title: "ASU and CAP LTER research data solutions"
author:
  - name: Stevan R. Earl
    affil: 1,2
    twitter: StevanEarl
    email: stevan.earl\'at\'asu.edu
    orcid: 0000-0002-4465-452X
    main: FALSE
  - name: Philip Tarrant
    affil: 2
    email: philip.tarrant\'at\'asu.edu
    orcid: 0000-0002-0600-5115
    main: FALSE
affiliation:
  - num: 1
    address: Julie Ann Wrigley Global Institute of Sustainability and Innovation, Arizona State University, Tempe, AZ 85287-5402
  - num: 2
    address: Knowledge Enterprise Research Data Management Office, Arizona State University, Tempe, AZ 85287-5402
column_numbers: 3
column_margins: "0.5in"
columnline_col: "#008080"
logoright_name: 'assets/figures/cap-asm-im-qr.png'
logoleft_name: 'assets/figures/NSF_4-Color_bitmap_Logo.png'
output: 
  posterdown::posterdown_html:
    self_contained: false
poster_height: "36in"
poster_width: "46in"
primary_colour: "#C88912"
secondary_colour: "#8C1D40"
accent_colour: "#800080"
title_textcol: "black"
author_textcol: "black"
affiliation_textcol: "black"
sectitle_textcol: "black"
sectitle2_textsize: "50pt"
sectitle2_textcol: "#8C1D40"
link_col: "#0000EE"
columnline_style: "none"
---





<style>
  .logo_left {
    width: 10%;
  }
  .logo_right {
    width: 10%;
  }
</style>

# getting started

## *overview*

The Arizona State University (ASU) Research Data Management (RDM) office, ASU Library, and the CAP LTER Information Manager offer research data management services and technology solutions for ASU research projects. This research data management team can assist with the preparation of data management plans, undertake technology needs assessments, provide subsidized computing resources and data storage, and assist with data publication.

## *data management planning*

- Many grant funding agencies now require proposals to include information regarding how research data will be collected, managed, published, and preserved. The RDM office can help you create a data management plan (DMP), and provide boilerplate DMP text that you can copy, paste, and modify for inclusion in your proposal.
- ASU supports the use of the Data Management Plan Tool (DMPTool https://dmptool.org/), a web-dased authoring tool will guide you through the process of creating a data management plan including templates for many funding agencies.

<img src="assets/figures/dmp_tool_with_text.png" title="plot of chunk DMPTool" alt="plot of chunk DMPTool" width="50%" style="display: block; margin: auto;" />

- Regardless of whether a DMP is required, investigators should think critically about how to manage their research data. The RDM office can offer advice in this space.

## *ORCiD* 

- ORCiD is an independent, non-profit organization that provides a persistent identifier (an ORCiD ID) that distinguishes you from other researchers and a mechanism for linking your research outputs and activities to your ORCiD. ORCiD is integrated into many systems used by publishers, funders, institutions, and other research-related services. If you already have an ORCiD, you can affiliate your account with ASU, which will allow you to access your ORCiD profile using single sign-on.
- Registering for an ORCiD is a simple process that provides benefits. Use your ORCiD when you publish articles and datasets; register for meetings; perform peer reviews; apply for grants; create email signatures, web pages, CVs, and more.

# research support

## *get organized*

Sound project organization is essential to successful research.

- Version control tools, such as Git coupled with GitHub  or GitLab , are immensely valuable for managing projects. 
- The Open Science Framework is a free, open platform from the Center for Open Science to manage research projects and enable collaboration.

<img src="assets/figures/osf.jpeg" title="plot of chunk OSF" alt="plot of chunk OSF" width="25%" style="display: block; margin: auto;" />

- Consider also an Electronic Research Notebook (ERN), often referred to as an electronic laboratory notebook, that provides a multi-functional data manager that can quickly and accurately import protocols, notes, observations, and other research data to organize information in one place. Embedded links to data files and other information help ensure that resources are not lost or misplaced.

<img src="assets/figures/LA_BetterScience_Logo.png" title="plot of chunk labarchives" alt="plot of chunk labarchives" width="50%" style="display: block; margin: auto;" />

- ASU has licensed LabArchives as our ERN solution, and offers the product at no cost to faculty, staff, and students for research activities.

## *finding data*

The RDM office can help investigators to find data, such as through the CAP LTER data portal. All CAP LTER project data are publicly available. Resources include data from current and historic long-term monitoring programs, and investigator-provided data.

<img src="assets/figures/cap_data_portal.png" title="plot of chunk cap-data-portal" alt="plot of chunk cap-data-portal" width="80%" style="display: block; margin: auto;" />

